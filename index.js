var Rx = require('rx');

Rx.Observable.just('Hello World!').subscribe(function(value){
  console.log(value);
});

//Creating Observables

//create Observables.

var observable = Rx.Observable.create(function(observer) {
  observer.onNext('Simon');
  observer.onNext('Jen');
  observer.onNext('Sergi');
  observer.onCompleted();
});



//we will merge two different Observables that use interval to produce values at different intervals.

var a = Rx.Observable.interval(200).map(function(i) {
  return 'A' + i;
});

var b = Rx.Observable.interval(100).map(function(i){
  return 'B' + i;
});

//Rx.Observable.merge(a, b).subscribe(function(x){
//  console.log(x);
//});


//Basic Sequence Operators

//Map

var logValue = function(val) {
  console.log(val);
};

var src = Rx.Observable.range(1, 5);
var upper = src.map(function(name) {
  return name * 2;
});

upper.subscribe(logValue);

//Filter

var isEven = (function(val) {
  return val % 2 !== 0;
});

var src = Rx.Observable.range(1, 5);
var even = src.filter(isEven);

even.subscribe(logValue);

//Reduce

var src = Rx.Observable.range(1, 5);
var sum = src.reduce(function(acc, x){
  return acc + x;
});

sum.subscribe(logValue);

//Aggregate

var avg = Rx.Observable.range(0, 5)
  .reduce(function(prev, cur){
    return {
      sum: prev.sum + cur,
      count: prev.count + 1
    };
  }, {sum: 0, count: 0 })
  .map(function(o){
    return o.sum/ o.count;
  });

var subscription = avg.subscribe(function(x){
  console.log('Average is: ', x);
});

// FlatMap

function concatAll(source) {
  return source.reduce(function(a, b) {
    return a.concat(b);
  });
}

console.log(concatAll([[0,1,2],[3,4,5],[6,7,8]]));

//Canceling Sequences

//Explicit Cancellation: The Disposable
/*
var counter = Rx.Observable.interval(1000);

var subscription1 = counter.subscribe(function(i) {
  console.log('Subscription 1:', i);
});

var subscription2 = counter.subscribe(function(i) {
  console.log('Subscription 2:', i);
});

setTimeout(function() {
  console.log('Canceling subscription2!');
  subscription2.dispose();
}, 2000);
*/

//Implicit Cancellation: By Operator

var p = new Promise(function(resolve, reject) {
  setTimeout(resolve, 5000);
});
p.then(function() {
console.log('Potential side effect!');
});
var subscription = Rx.Observable.fromPromise(p).subscribe(function(msg) {
console.log('Observable resolved!');
});
subscription.dispose();

// The onError Handler

function getJson(arr) {
  return Rx.Observable.from(arr).map(function(str) {
    var parsedJson = JSON.parse(str);
    return parsedJson;
  });
}


getJson([
  '{"1":1, "2": 2}',
  '{"sucess: true}',
  '{"enabled": true}'
]).subscribe(
  function(json) {
    console.log('Parsed JSON: ', json);
  },
  function(err) {
    console.log(err.message);
  }
);

// Catching Errors

function getJson(arr) {
  return Rx.Observable.from(arr).map(function(str) {
    var parsedJson = JSON.parse(str);
    return parsedJson;
  });
}

var caught = getJson(['{"1"; 1, "2": 2}', '{"1: 1}']).catch(
  Rx.Observable.return({
    error: 'There was an error parsing JSON'
  })
);

caught.subscribe(
  function(json) {
    console.log('Parsed JSON: ', json);
  },
  function(e) {
    console.log('ERROR', e.message);
  }
);

















