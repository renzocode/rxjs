# Rxjs

## Visualizing Observables


![alt text](./images/marble-diagram.png)


## Creating Observables

create Observables.

```js
var Rx = require('rx');
var observable = Rx.Observable.create(function(observer) {
  observer.onNext('Simon');
  observer.onNext('Jen');
  observer.onNext('Sergi');
  observer.onCompleted();
});
```
## Basic Sequence Operatorsa

### Merge

we will merge two different Observables that use interval to produce values at different intervals.


```js
var Rx = require('rx');
var a = Rx.Observable.interval(200).map(function(i) {
  return 'A' + i;
});

var b = Rx.Observable.interval(100).map(function(i){
  return 'B' + i;
});

Rx.Observable.merge(a, b).subscribe(function(x){
  console.log(x);
});

```
B0
A0
B1
B2
A1
B3
B4
A2
B5
B6
A3

![alt text](./images/merge-operator.png)

### Map

It returns a new Observable with the transformed values.

```js
var Rx = require('rx');
var logValue = function(val) {
  console.log(val);
};

var src = Rx.Observable.range(1, 5);
var upper = src.map(function(name) {
  return name * 2;
});

upper.subscribe(logValue);
```
2
4
6
8
10

![alt text](./images/map-operator.png)

### Filter

It returns an Observable sequence of all the elements for wich the function returned true.

```js
var Rx = require('rx');
var logValue = function(val) {
  console.log(val);
};

var isEven = (function(val) {
  return val % 2 !== 0;
});

var src = Rx.Observable.range(1, 5);
var even = src.filter(isEven);

even.subscribe(logValue);

```
1
3
5

![alt text](./images/filter-operator.png)


### Reduce

takes an Observable and returns a new one that always contains a single item.

```js
var Rx = require('rx');
var src = Rx.Observable.range(1, 5);
var sum = src.reduce(function(acc, x){
  return acc + x;
});

sum.subscribe(logValue);

```

15

![alt text](./images/reduce-operator.png)

### Aggregate Operators

Aggregate operators process a sequence and return a single value.

```js
var Rx = require('rx');
var avg = Rx.Observable.range(0, 5)
  .reduce(function(prev, cur){
    return {
      sum: prev.sum + cur,
      count: prev.count + 1
    };
  }, {sum: 0, count: 0 })
  .map(function(o){
    return o.sum/ o.count;
  });

var subscription = avg.subscribe(function(x){
  console.log('Average is: ', x);
});

```

### FlatMap

takes an Observable A whose elements are also Observables, and returns an Observable with the flattened values of A's child Observables.

```js
function concatAll(source) {
  return source.reduce(function(a, b) {
    return a.concat(b);
  });
}

console.log(concatAll([[0,1,2],[3,4,5],[6,7,8]]));
```
[0, 1, 2, 3, 4, 5, 6, 7, 8]

![alt text](./images/flatmap-operator.png)

## Cancelling Sequences

### Explicit Cancellation: The Disposable

we can cancel a running Observable.

```js

var Rx = require('rx');
var counter = Rx.Observable.interval(1000);

var subscription1 = counter.subscribe(function(i) {
  console.log('Subscription 1:', i);
});

var subscription2 = counter.subscribe(function(i) {
  console.log('Subscription 2:', i);
});


setTimeout(function() {
  console.log('Canceling subscription2!');
  subscription2.dispose();
}, 2000);

```

Subscription 1: 0  
Subscription 2: 0  
Canceling subscription2!  
Subscription 1: 1  
Subscription 1: 2  
Subscription 1: 3  
Subscription 1: 4  
Subscription 1: 5  

### Implicit Cancellation: By Operator

we attempt to cancel a subscription to an Observable
that wraps a promise p.

```js

var p = new Promise(function(resolve, reject) {
  setTimeout(resolve, 5000);
});
p.then(function() {
console.log('Potential side effect!');
});
var subscription = Rx.Observable.fromPromise(p).subscribe(function(msg) {
console.log('Observable resolved!');
});
subscription.dispose();

```
Potential side effect!

## Handling Errors
### The onError Handler

It is the key to effectively handling errors in Observable sequences.

```js
var Rx = require('rx');
function getJson(arr) {
  return Rx.Observable.from(arr).map(function(str) {
    var parsedJson = JSON.parse(str);
    return parsedJson;
  });
}

getJson([
  '{"1":1, "2": 2}',
  '{"sucess: true}',
  '{"enabled": true}'
]).subscribe(
  function(json) {
    console.log('Parsed JSON: ', json);
  },
  function(err) {
    console.log(err.message);
  }
);

```
Parsed JSON:  { '1': 1, '2': 2 }  
Unexpected end of JSON input  

### Catching Errors

how to detect that an error has happened and do something with that information.

```js
var Rx = require('rx');
function getJson(arr) {
  return Rx.Observable.from(arr).map(function(str) {
    var parsedJson = JSON.parse(str);
    return parsedJson;
  });
}

var caught = getJson(['{"1"; 1, "2": 2}', '{"1: 1}']).catch(
  Rx.Observable.return({
    error: 'There was an error parsing JSON'
  })
);

caught.subscribe(
  function(json) {
    console.log('Parsed JSON: ', json);
  },
  function(e) {
    console.log('ERROR', e.message);
  }
);

```

Parsed JSON:  { error: 'There was an error parsing JSON' }

![alt text](./images/catch-operator.png)

